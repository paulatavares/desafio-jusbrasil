# Referência

[Desafio para o Processo Seletivo de Estágio na Jusbrasil](https://github.com/jusbrasil/careers/blob/master/frontend/internship/desafio.md)

# Requisitos

- Docker
- Node v11.4

# O que foi feito

- Implementação de API em Node
- Utilização de back-end com ElasticSearch
- Aplicação Web em React

# Como executar

Subir dependência (ElastichSearch) do projeto via docker-compose:

```
docker-compose up
```

Terminada a etapa anterior, instalar as dependências para utilização da API:

```
yarn
```

Execução da API:

```
yarn dev
```

Para iniciar a aplicação web é nescessário navegar até o diretório 'search-app':

```
cd search-app
```

Instalação de dependências:

```
yarn
```

Para rodar a aplicação:

```
yarn start
```

# Alimentando a base

Com a aplicação em execução adicionar novos itens com:

```
curl -X POST http://localhost:3000/entities -d '{"title": "Some title", "type": "TOPIC"}'
```
