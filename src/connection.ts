import elasticsearch from "elasticsearch";

const schema = {
  title: { type: "completion" },
  type: { type: "text" }
};

// TODO: check if exist index

const client = new elasticsearch.Client({
  host: "localhost:9200",
  log: "trace"
});

client.ping(
  {
    requestTimeout: 30000
  },
  error => {
    if (error) {
      console.log("elastichsearch cluster is down!");
    } else {
      console.log("All is well");
    }
  }
);

client.indices.create(
  {
    index: "searchengine"
  },
  (error, response) => {
    if (error) {
      console.log(error);
    } else {
      console.log("created a new index", response);
      client.indices.putMapping({
        index: "searchengine",
        type: "searchType",
        body: { properties: schema }
      });
    }
  }
);

export default client;
