import server from "./server";

server.listen(3000, () => {
  console.log(`[SERVER] Running at http://elasticsearch:3000`);
});
