import express from "express";
import cors from "cors";
import client from "./connection";
import bodyParser from "body-parser";

const server = express();

var corsOptions = {
  origin: "http://localhost:8080",
  optionsSuccessStatus: 200
};

server.use(cors(corsOptions));
server.options("*", cors(corsOptions)); // include before other routes

server.get("/", (_, res) => {
  res.send("Hello World!");
});

// Type '*/*' every data will be parsed like a JSON
server.use(bodyParser.json({ type: "*/*" }));

server.post("/entities", (req, res) => {
  console.log(req.body);
  if (!req.body.title) {
    return res.status(400).send({
      success: "false",
      message: "title is required"
    });
  } else if (!req.body.type) {
    return res.status(400).send({
      success: "false",
      message: "type is required"
    });
  }
  client.index(
    {
      index: "searchengine",
      type: "searchType",
      body: {
        title: {
          input: req.body.title
        },
        type: req.body.type
      }
    },
    (error, resp) => {
      console.log(resp);
      console.log(error);
    }
  );

  return res.status(200).send({
    success: "true",
    message: "searchable added successfully"
  });
});

server.get("/entities", (req, res) => {
  if (!req.query.q) {
    return res.status(400).send({
      success: "false",
      message: "you need a query word"
    });
  }

  let body = {
    size: 10,
    from: 0,
    suggest: {
      titleSuggest: {
        prefix: req.query.q,
        completion: {
          field: "title"
        }
      }
    }
  };

  client
    .search({ index: "searchengine", body: body, type: "searchType" })
    .then((
      results: any // The lib doesn't has type for suggest API yet
    ) =>
      res.send(
        results.suggest.titleSuggest[0].options.map((result: any) => ({
          title: result._source.title.input,
          type: result._source.type
        }))
      )
    )
    .catch(error => {
      console.log(error);
      res.send({ status: res.status(500) });
    });
  return undefined;
});

export default server;
