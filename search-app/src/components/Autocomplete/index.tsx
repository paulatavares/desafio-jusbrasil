import React from "react";
import Downshift from "downshift";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import MenuItem from "@material-ui/core/MenuItem";
import "./style.css";

interface IState {
  inputValue: string;
  selectedItens: { title: string }[];
}
interface IProps {
  searchItens: (x: { searchbleWord: string }) => any;
}

class Autocomplete extends React.Component<IProps, IState> {
  state: IState = {
    inputValue: "",
    selectedItens: []
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const inputValue = event.target.value;
    this.setState({ inputValue });
    if (inputValue) {
      const selectedItens = this.props
        .searchItens({
          searchbleWord: inputValue
        })
        .then((selectedItens: { title: string }[]) => {
          this.setState({ selectedItens });
          console.log(selectedItens);
        });
    }
  };

  render() {
    let { inputValue, selectedItens } = this.state;
    return (
      <div className="container">
        <Downshift
          onChange={selection => this.setState({ inputValue: selection.title })}
          itemToString={item => (item ? item.value : "")}
        >
          {({
            getInputProps,
            getItemProps,
            getMenuProps,
            highlightedIndex,
            selectedItem
          }) => (
            <div>
              <TextField
                {...getInputProps()}
                id="searchInput"
                label="Search"
                className="textField"
                margin="normal"
                onChange={this.handleChange}
                value={inputValue}
              />
              <Paper className="root" {...getMenuProps()}>
                {inputValue
                  ? selectedItens.map((item, index) => (
                      <MenuItem
                        {...getItemProps({
                          key: index,
                          index,
                          item,
                          style: {
                            backgroundColor:
                              highlightedIndex === index
                                ? "lightgray"
                                : "white",
                            fontWeight:
                              selectedItem === item ? "bold" : "normal"
                          }
                        })}
                      >
                        {item.title}
                      </MenuItem>
                    ))
                  : null}
              </Paper>
            </div>
          )}
        </Downshift>
      </div>
    );
  }
}

export default Autocomplete;
