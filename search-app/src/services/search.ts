import api from "./api";

type newItemType = {
  title: string;
  type: string;
};

type searchbleItenType = {
  searchbleWord: string;
};

export async function insertSearchbleItens(newItem: newItemType) {
  const url = "/entities";
  const response = await api.post(url, newItem);
  return response.data;
}

export async function searchItens(searchbleIten: searchbleItenType) {
  const { searchbleWord } = searchbleIten;
  const url = `/entities?q=${searchbleWord}`;
  const response = await api.get(url);
  return response.data;
}
