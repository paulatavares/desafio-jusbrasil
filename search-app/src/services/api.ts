import { create } from "apisauce";

const api = create({
  baseURL: "http://localhost:3000",
  timeout: 20000
});

export default api;
