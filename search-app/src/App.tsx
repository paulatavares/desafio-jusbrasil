import React, { Component } from "react";
import { searchItens } from "./services/search";
import Autocomplete from "./components/Autocomplete";

class App extends Component {
  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <Autocomplete searchItens={searchItens} />
      </div>
    );
  }
}

export default App;
